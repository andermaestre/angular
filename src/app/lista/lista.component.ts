import { Component } from '@angular/core';
import { ListaMdl } from '../modelos/listaMdl';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent {
  model: ListaMdl= {
    titulo: "Login",
    lista: [
      {
        nombre: "Señor",
        apellido1: "1",
        apellido2: "1",
        usuario: "11",
        fNac: new Date
      },
      {
        nombre: "Señor",
        apellido1: "2",
        apellido2: "2",
        usuario: "22",
        fNac: new Date
      },
      {
        nombre: "Señor",
        apellido1: "3",
        apellido2: "3",
        usuario: "33",
        fNac: new Date
      },
      {
        nombre: "Señor",
        apellido1: "4",
        apellido2: "4",
        usuario: "44",
        fNac: new Date
      },
      {
        nombre: "Señor",
        apellido1: "5",
        apellido2: "5",
        usuario: "55",
        fNac: new Date
      },
    ]
  };
}
