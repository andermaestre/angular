import { Component } from '@angular/core';
import { Modelo } from '../modelos/loginMdl';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  model: Modelo= {
    titulo: "Login",
    usuario: "Señor",
    saludo: "Hola!!"
  };
}
